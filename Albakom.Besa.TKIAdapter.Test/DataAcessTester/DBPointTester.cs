﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Albakom.Besa.TKIAdapter.Test.DataAcessTester
{
    public class DBPointTester
    {
        [Fact]
        public async Task TestPointCoordinate_Pass()
        {
            using (TKIEntities db = new TKIEntities())
            {
                Int32 pointID = 33580;

                TKIPoint point = db.TKIPoints.First(x => x.FID == pointID);
                GPSCoordinate coordinate = await TKIPoint.GetCoordinate(db, point.FID);

                Assert.NotNull(coordinate);
            }
        }

        [Fact]
        public async Task Haustester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                Int32 cabinetId = 207438;

                await db.GetBuildingConnectedByStreetCabinet(cabinetId);
            }
        }

        [Fact]
        public async Task MuffenTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String cabinetId = "27309";
                ProjektAdapterSplicePrintResultModel result = await db.GetAllSpicesByBranchableId(cabinetId);
                IEnumerable<ProjektAdapterSplicePrintModel> spliceResult = result.Splices;

                IEnumerable<ProjektAdapterSplicePrintModel> resultToPrint = spliceResult.Where(x => x.Type != SpliceTypes.LoopUntouched);

            }
        }

        [Fact]
        public async Task GetMainCableForBranchable()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String cabinetId = "27309";
                ProjectAdapterMainCableForBranchableResult result = await db.GetMainCableForBranchable(cabinetId);
            }
        }

        

        [Fact]
        public async Task GetBranchableCableTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String cabinetId = "27309";
                await db.GetCableByBranchableId(cabinetId);
            }
        }

        [Fact]
        public async Task GetAllPoPsTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                await db.GetAllPoPs();
            }
        }

        [Fact]
        public async Task GetMainCablesPerPop()
        {
            using (TKIEntities db = new TKIEntities())
            {
                var result = await db.GetCableOverview();
            }
        }


        [Fact]
        public async Task GetMainCableStartingFromPopTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String popId = "337375";
                await db.GetMainCableStartingFromPop(popId);
            }
        }

        [Fact]
        public async Task GetConnectionByCableIdTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String cableId = "731826";
                await db.GetConnectionByCableId(cableId);
            }
        }

        [Fact]
        public async Task BuildingPathTester()
        {
            using (TKIEntities db = new TKIEntities())
            {
                String buildingID = "29774";
                await db.GetUpdateBuildingWithCableInfo(buildingID);

            }
        }
    }
}
