﻿using Albakom.Besa.TKIAdapter.Contracts.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.TKIAdapter.Contracts.Models;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Net.Mime;
using OfficeOpenXml.Style;

namespace Albakom.Besa.TKIAdapter.Implementation
{
    public class ProjectDataAdapter : IProjectDataAdapter
    {
        #region Fields

        private IDataAccess _storage;

        #endregion

        #region Constructor

        public ProjectDataAdapter(IDataAccess access)
        {
            _storage = access;
        }

        #endregion

        #region Methods

        public Task<IEnumerable<ProjectAdapterBuildingModel>> GetAllBuildings()
        {
            return _storage.GetAllBuildings();
        }

        public Task<IEnumerable<ProjectAdapterCabinetModel>> GetAllCabinets(Int32? start, Int32? amount)
        {
            return _storage.GetAllCabinets(start,amount);
        }

        public Task<IEnumerable<ProjectAdapterSleeveModel>> GetAllSleeves(Int32? start, Int32? amount)
        {
            return _storage.GetAllSleeves(start,amount);
        }

        public Task<IEnumerable<ProjectAdapterBranchableOverviewModel>> GetAllBranchables()
        {
            return _storage.GetAllBranchables();
        }

        public Task<ProjektAdapterSplicePrintResultModel> GetAllSpicesByBranchableId(String projectAdapterId)
        {
            return _storage.GetAllSpicesByBranchableId(projectAdapterId);
        }

        public Task<IEnumerable<ProjectAdapterCableTypeModel>> GetAllCableTypes()
        {
            return _storage.GetAllCableTypes();
        }

        public Task<ProjectAdapterUpdateBuldingModel> GetUpdateBuildingWithCableInfo(String adapterId)
        {
            return _storage.GetUpdateBuildingWithCableInfo(adapterId);
        }

        public Task<ProjectAdapterUpdateBuildingNameModel> GetUpdateBuildingName(String adapterId)
        {
            return _storage.GetUpdateBuildingName(adapterId);
        }


        public Task<IEnumerable<ProjectAdapterCableModel>> GetCableByBranchableId(String branchableProjectAdapterId)
        {
            return _storage.GetCableByBranchableId(branchableProjectAdapterId);

        }

        public Task<IEnumerable<ProjectAdapterSpliceModel>> GetSplicesForBranchable(String projectAdapterId)
        {
            return _storage.GetSplicesForBranchable(projectAdapterId);

        }

        public Task<IEnumerable<ProjectAdapterPoPModel>> GetAllPoPs()
        {
            return _storage.GetAllPoPs();

        }

        public Task<IEnumerable<String>> GetMainCableStartingFromPop(String popProjectAdapterId)
        {
            return _storage.GetMainCableStartingFromPop(popProjectAdapterId);

        }

        public Task<IEnumerable<ProjectAdapterPatchModel>> GetConnectionByCableId(String projectAdatperCableID)
        {
            return _storage.GetConnectionByCableId(projectAdatperCableID);
        }


        public Task<ProjectAdapterMainCableForBranchableResult> GetMainCableForBranchable(String branchableProjectAdapterId)
        {
            return _storage.GetMainCableForBranchable(branchableProjectAdapterId);
        }

        public async Task<Stream> ExpportSpliceOverviewAsCSV(String projectAdapterId)
        {
            IEnumerable<ProjektAdapterSplicePrintModel> splices = (await _storage.GetAllSpicesByBranchableId(projectAdapterId)).Splices;
            List<ProjektAdapterSplicePrintModel> splicesToPrint = splices.Where(x => x.Type != SpliceTypes.LoopUntouched).ToList();

            Char seperationChar = ';';

            MemoryStream stream = new MemoryStream(4* 1024);
            StreamWriter writer = new StreamWriter(stream);

            foreach (ProjektAdapterSplicePrintModel item in splicesToPrint)
            {
                writer.Write(item.Fiber1.Name);
                writer.Write(seperationChar);

                writer.Write(String.Format("{0}-{1}", item.Fiber1.BundleNumber, item.Fiber1.NumberinBundle));
                writer.Write(seperationChar);

                writer.Write(item.Fiber1.NumberinBundle);
                writer.Write(seperationChar);

                writer.Write(item.Fiber1.Color);
                writer.Write(seperationChar);

                if(item.Fiber2 != null)
                {
                    writer.Write(item.Fiber2.Name);
                    writer.Write(seperationChar);

                    writer.Write(String.Format("{0}-{1}", item.Fiber2.BundleNumber, item.Fiber2.NumberinBundle));
                    writer.Write(seperationChar);

                    writer.Write(item.Fiber2.NumberinBundle);
                    writer.Write(seperationChar);

                    writer.Write(item.Fiber2.Color);
                    writer.Write(seperationChar);
                }

                writer.WriteLine();
            }

            writer.Flush();
            stream.Position = 0;

            return stream;
        }

        public async Task<Stream> ExpportSpliceOverviewAsXlsx_old(string projectAdapterId)
        {
            List<string> headings = new List<string> { "Kabel 1 Alias", "Faser 1", "Nr 1", "Faser/BA", "Kabel 2 Alias", "Faser 2", "Nr 2", "Faser/BA", "Spleißplatz", "Kassette", "Bemerkung"};
            IEnumerable<ProjektAdapterSplicePrintModel> splices = (await _storage.GetAllSpicesByBranchableId(projectAdapterId)).Splices;
            List<ProjektAdapterSplicePrintModel> splicesToPrint = splices.Where(x => x.Type != SpliceTypes.LoopUntouched).ToList();

            DataTable dt = new DataTable();
           

            dt.Merge(await _storage.GetAllSpicesByBranchableIdAsDataTable(projectAdapterId, headings));

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Spleißübersicht");


                ws.Cells["A1"].LoadFromDataTable(dt, true);

                MemoryStream stream = new MemoryStream(4 * 1024);
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(pck);
                writer.WriteLine();

                writer.Flush();
                stream.Position = 0;

                return stream;
            }
        }

        public async Task<Stream> ExpportSpliceOverviewAsXlsx(string projectAdapterId)
        {
            List<string> headings = new List<string> { "Kabel 1 Alias", "Faser 1", "Nr 1", "Faser/BA", "Kabel 2 Alias", "Faser 2", "Nr 2", "Faser/BA", "Spleißplatz", "Kassette", "Bemerkung" };

            ProjektAdapterSplicePrintResultModel result = await _storage.GetAllSpicesByBranchableId(projectAdapterId);

            IEnumerable<ProjektAdapterSplicePrintModel> splices = result.Splices;
            List<ProjektAdapterSplicePrintModel> splicesToPrint = splices.Where(x => x.Type != SpliceTypes.LoopUntouched).ToList();

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet workSheet = pck.Workbook.Worksheets.Add("Spleißübersicht");

                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = result.Branchable.Name;
                workSheet.Cells["A1:K1"].Merge = true;

                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                workSheet.Row(2).Style.Font.Bold = true;

                for (int i = 1; i < headings.Count + 1; i++)
                {
                    workSheet.Cells[2, i].Value = headings[i-1];
                }

                fillWorkSheetWithData(workSheet, splicesToPrint);
                using (ExcelRange range = workSheet.Cells[2, 1, workSheet.Dimension.End.Row, workSheet.Dimension.End.Column])
                {
                    var table = workSheet.Cells[range.ToString()];
                    table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    foreach (var cell in range)
                    {
                        if (cell.Style.Border.Top.Style == ExcelBorderStyle.None)
                        {
                            cell.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        }
                    }
                }

                
                workSheet.Cells.AutoFitColumns();
                // Druckeinstellungen
                workSheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$2");
                workSheet.PrinterSettings.PaperSize = ePaperSize.A4;
                workSheet.PrinterSettings.Orientation = eOrientation.Portrait;                               
                workSheet.PrinterSettings.LeftMargin = .05M;
                workSheet.PrinterSettings.RightMargin = .05M;
                workSheet.PrinterSettings.TopMargin = .05M;
                workSheet.PrinterSettings.FooterMargin = .05M;
                workSheet.PrinterSettings.Scale = 80;
                

                MemoryStream memoryStream = new MemoryStream(4 * 1024);
                pck.SaveAs(memoryStream);
                memoryStream.Position = 0;

                return memoryStream;
            }
        }

        private void fillWorkSheetWithData(ExcelWorksheet workSheet, List<ProjektAdapterSplicePrintModel> splicesToPrint)
        {
            int recordIndex = 3;

            var splicesToPrintOrdered = splicesToPrint.OrderBy(x => x.TrayNumber);

            string tempKvzName1 = string.Empty;
            string currentAlias1 = string.Empty;
            int currentTrayNumber = 0;
            foreach (ProjektAdapterSplicePrintModel item in splicesToPrintOrdered)
            {
                currentAlias1 = item.Fiber1.Name;
                if (currentAlias1.Trim().ToLowerInvariant().StartsWith("kvz"))
                {
                    if (item.Fiber1.Name == tempKvzName1)
                    {
                        //Kabel 1 Alias
                        workSheet.Cells[recordIndex, 1].Value = string.Empty;
                    }
                    else
                    {
                        //Kabel 1 Alias
                        workSheet.Cells[recordIndex, 1].Value = item.Fiber1.Name;
                        tempKvzName1 = item.Fiber1.Name;
                    }

                    // Faser 1
                    workSheet.Cells[recordIndex, 2].Value = item.Fiber1.NumberinBundle.ToString();
                    // Nr. 1
                    workSheet.Cells[recordIndex, 3].Value = item.Fiber1.FiberNumber.ToString();
                    // Faser / BA
                    workSheet.Cells[recordIndex, 4].Value = getFaserBaColor(item.Fiber1.Color);
                }
                else
                {
                    // Kabel 1 Alias
                    workSheet.Cells[recordIndex, 1].Value = item.Fiber1.Name;
                    // Faser 1
                    workSheet.Cells[recordIndex, 2].Value = String.Format("{0}-{1}", item.Fiber1.BundleNumber, item.Fiber1.NumberinBundle);
                    // Nr 1
                    workSheet.Cells[recordIndex, 3].Value = item.Fiber1.FiberNumber;
                    // Faser / BA
                    workSheet.Cells[recordIndex, 4].Value = item.Fiber1.Color;


                    tempKvzName1 = item.Fiber1.Name;
                }

                if (item.Fiber2 != null)
                {
                    // Kabel 2 Alias
                    workSheet.Cells[recordIndex, 5].Value = item.Fiber2.Name;
                    
                    if (item.Fiber2.Name.Trim().ToLowerInvariant().StartsWith("kvz"))
                    {
                        // Faser 2
                        workSheet.Cells[recordIndex, 6].Value = String.Format("{0}", item.Fiber2.NumberinBundle);
                        // Faser/BA
                        workSheet.Cells[recordIndex, 8].Value = getFaserBaColor(item.Fiber2.Color);
                    }
                    else
                    {
                        // Faser 2
                        workSheet.Cells[recordIndex, 6].Value = String.Format("{0}-{1}", item.Fiber2.BundleNumber, item.Fiber2.NumberinBundle);
                        // Faser/BA
                        workSheet.Cells[recordIndex, 8].Value = item.Fiber2.Color;
                    }
                    // Nr. 2
                    workSheet.Cells[recordIndex, 7].Value = item.Fiber2.NumberinBundle;
                }

                // Spleißplatz
                workSheet.Cells[recordIndex, 9].Value = (item.NumberInTray != 0 ? item.NumberInTray.ToString(): string.Empty);
                // Kassette
                workSheet.Cells[recordIndex, 10].Value = item.TrayNumber;

                if (currentTrayNumber != item.TrayNumber)
                {
                    currentTrayNumber = item.TrayNumber;
                    if (recordIndex > 3)
                    {
                        // Linie zum Abgrenzen
                        for (int i = 1; i < 12; i++)
                        {
                            workSheet.Cells[recordIndex, i].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                        }
                    }
                }

                recordIndex++;
            }
        }

        private string getFaserBaColor(string colorString)
        {
            if (!colorString.ToLowerInvariant().Contains("faser/ba:"))
            {
                return colorString;
            }
            string color = string.Empty;
            color = colorString.Split(':')[1];
            color = color.Split('/')[0];
            return color.Trim();
        }

        #endregion
    }
}
