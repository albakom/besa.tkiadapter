//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Albakom.Besa.TKIAdapter.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class TKIColors
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TKIColors()
        {
            this.TKIDucts = new HashSet<TKIDucts>();
        }
    
        public int ID { get; set; }
        public string VALUE { get; set; }
        public string RGB_VALUE { get; set; }
        public Nullable<int> ORDER_ID { get; set; }
        public Nullable<byte> R_VALUE { get; set; }
        public Nullable<byte> G_VALUE { get; set; }
        public Nullable<byte> B_VALUE { get; set; }
        public Nullable<bool> DASHED { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TKIDucts> TKIDucts { get; set; }
    }
}
