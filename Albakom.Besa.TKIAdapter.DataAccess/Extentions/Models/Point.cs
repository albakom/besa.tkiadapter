﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.DataAccess
{
    public partial class TKIPoint
    {
        public static  async Task<GPSCoordinate> GetCoordinate(DbContext ctx, Int32 pointId)
        {
            OracleParameter[] parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("POINTID", pointId);
            String query =
               "SELECT (SDO_CS.TRANSFORM(p.GEOM, 4326)).SDO_POINT.X as Longitude, (SDO_CS.TRANSFORM(p.GEOM, 4326)).SDO_POINT.Y as Latitude FROM TKI_TETEROW.TC_POINT p WHERE p.FID = :PointID";

            GPSCoordinate result = await ctx.Database.SqlQuery<GPSCoordinate>(query, parameters).FirstAsync();
            return result;
        }
    }
}
