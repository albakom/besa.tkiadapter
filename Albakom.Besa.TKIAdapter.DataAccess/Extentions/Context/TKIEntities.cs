﻿using Albakom.Besa.TKIAdapter.Contracts.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.TKIAdapter.Contracts.Models;
using System.Data;

namespace Albakom.Besa.TKIAdapter.DataAccess
{
    public partial class TKIEntities : IDataAccess
    {
        private const Int32 _blankFiberSpliceTrayModelID = 348529;
        private const Int32 _depositPseudoTrayNumber = 999;
        private const Int32 _popStreetCabinetType = 337378;


        private class ConnectionPathElement
        {
            public TKIConnections Connection { get; set; }
            public Int32 CurrentItemId { get; set; }
        }

        public async Task<IEnumerable<ProjectAdapterBuildingModel>> GetAllBuildings()
        {
            var result = await (from building in TKIBuildings
                                select new
                                {
                                    CommercialUnits = building.CommercialUnits.HasValue == false ? 0 : building.CommercialUnits.Value,
                                    Coordinate = new GPSCoordinate(),
                                    HouseholdUnits = building.HouseholdUnits.HasValue == false ? 0 : building.HouseholdUnits.Value,
                                    AdapterId = building.FID.ToString(),
                                    StreetName = building.Street.Name,
                                    StreetNumber = building.HouseNumber,
                                }).ToListAsync();

            return result.Select(x => new ProjectAdapterBuildingModel
            {
                AdapterId = x.AdapterId,
                CommercialUnits = x.CommercialUnits,
                Coordinate = x.Coordinate,
                HouseholdUnits = x.HouseholdUnits,
                Street = String.Format("{0} {1}", x.StreetName, x.StreetNumber)
            });
        }

        private async Task<ProjectAdapterBuildingModel> GetBuildingById(Int32 buildingId)
        {
            var result = await (from building in TKIBuildings
                                where building.FID == buildingId
                                select new
                                {
                                    CommercialUnits = building.CommercialUnits.HasValue == false ? 0 : building.CommercialUnits.Value,
                                    Coordinate = new GPSCoordinate(),
                                    HouseholdUnits = building.HouseholdUnits.HasValue == false ? 0 : building.HouseholdUnits.Value,
                                    AdapterId = building.FID.ToString(),
                                    StreetName = building.Street.Name,
                                    StreetNumber = building.HouseNumber,
                                    Suffix = building.SUFFIX,
                                }).ToListAsync();

            return result.Select(x => new ProjectAdapterBuildingModel
            {
                AdapterId = x.AdapterId,
                CommercialUnits = x.CommercialUnits,
                Coordinate = x.Coordinate,
                HouseholdUnits = x.HouseholdUnits,
                Street = (String.Format("{0} {1} {2}", x.StreetName, x.StreetNumber, String.IsNullOrEmpty(x.Suffix) == false ? x.Suffix : String.Empty)).TrimEnd()
            }).First();
        }

        public async Task<IEnumerable<ProjectAdapterCabinetModel>> GetAllCabinets(Int32? start, Int32? amount)
        {
            var filter = (from cabinet in TKIStreetCabinents
                          join point in TKIPoints on cabinet.FID equals point.FID_ATTR
                          orderby point.Name
                          select new
                          {
                              pointId = point.FID,
                              AdapterId = cabinet.FID,
                              Name = point.Name,
                          });

            if (start.HasValue == true)
            {
                filter = filter.Skip(start.Value);
            }

            if (amount.HasValue == true)
            {
                filter = filter.Take(amount.Value);
            }

            var result = await filter.ToListAsync();

            List<ProjectAdapterCabinetModel> items = new List<ProjectAdapterCabinetModel>();
            foreach (var item in result)
            {
                ProjectAdapterCabinetModel resultItem = new ProjectAdapterCabinetModel
                {
                    AdapterId = item.AdapterId.ToString(),
                    Name = item.Name,
                    Coordinate = await TKIPoint.GetCoordinate(this, item.pointId)
                };

                IEnumerable<ProjectAdapterBuildingModel> buildings = await GetBuildingConnectedByStreetCabinet(item.AdapterId);
                resultItem.Buildings = buildings;

                items.Add(resultItem);
            }

            return items;
        }

        public async Task<IEnumerable<ProjectAdapterSleeveModel>> GetAllSleeves(Int32? start, Int32? amount)
        {
            var filter = from cabinet in TKICleeves
                         join point in TKIPoints on cabinet.FID equals point.FID_ATTR
                         orderby point.Name
                         select new
                         {
                             pointId = point.FID,
                             AdapterId = cabinet.FID,
                             Name = point.Name,
                         };

            if (start.HasValue == true)
            {
                filter = filter.Skip(start.Value);
            }

            if (amount.HasValue == true)
            {
                filter = filter.Take(amount.Value);
            }

            var result = await filter.ToListAsync();

            List<ProjectAdapterSleeveModel> items = new List<ProjectAdapterSleeveModel>();
            foreach (var item in result)
            {
                ProjectAdapterSleeveModel resultItem = new ProjectAdapterSleeveModel
                {
                    AdapterId = item.AdapterId.ToString(),
                    Name = item.Name,
                    Coordinate = await TKIPoint.GetCoordinate(this, item.pointId)
                };

                IEnumerable<ProjectAdapterBuildingModel> buildings = await GetBuildingConnectedByStreetCabinet(item.AdapterId);
                resultItem.Buildings = buildings;

                items.Add(resultItem);
            }

            return items;
        }

        private async Task<ProjectAdapterColor> GetProjectColor(Int32 colorId)
        {
            TKIColors color = await TKIColors.FindAsync(colorId);
            if (color == null)
            {
                return new ProjectAdapterColor
                {
                    Name = "Not specified",
                    RGBHexValue = "#000000",
                };
            }

            return new ProjectAdapterColor
            {
                Name = color.VALUE,
                RGBHexValue = color.RGB_VALUE,
            };
        }

        private async Task FindBuildings(IEnumerable<Int32> connectionIds, ICollection<ConnectionPathElement> connectionPath, ICollection<ProjectAdapterBuildingModel> buildings)
        {
            foreach (Int32 itemId in connectionIds)
            {
                var tempResult = await TKIConnections.Where(x => x.FromId == itemId || x.ToId == itemId).ToListAsync();

                connectionPath.Add(new ConnectionPathElement
                {
                    CurrentItemId = itemId,
                });
                IEnumerable<Int32> nextIds = tempResult.Select(x => x.ToId).Union(tempResult.Select(x => x.FromId)).Except(connectionPath.Select(x => x.CurrentItemId));
                if (nextIds.Count() == 0)
                {
                    TKIConnections connection = tempResult.First();

                    Int32 pointId = connection.ToId;
                    Int32 lineId = connection.FromId;
                    if (connection.ClassTypeFrom == ClassTypes.Point)
                    {
                        pointId = connection.FromId;
                        lineId = connection.ToId;
                    }

                    TKIPoint point = await TKIPoints.FirstAsync(x => x.FID == pointId);
                    if (point.FID_BUILDING.HasValue == false) { continue; }

                    ProjectAdapterBuildingModel building = await GetBuildingById(point.FID_BUILDING.Value);
                    building.Coordinate = await TKIPoint.GetCoordinate(this, point.FID);
                    buildings.Add(building);

                    TKILines line = await TKILines.FirstAsync(x => x.FID == lineId);
                    if (line.ClassType == ClassTypes.Duct)
                    {
                        TKIDucts duct = await TKIDucts.FirstAsync(x => x.FID == line.FID_ATTR);
                        if (duct.ColorId.HasValue == false)
                        {
                            continue;
                        }

                        building.HouseConnectionColor = await GetProjectColor(duct.ColorId.Value);
                    }
                }
                else
                {
                    await FindBuildings(nextIds, connectionPath, buildings);
                }
            }
        }


        public async Task<IEnumerable<ProjectAdapterBuildingModel>> GetBuildingConnectedByStreetCabinet(Int32 cabinetId)
        {
            IEnumerable<Int32?> ductIds = await TKIPoints.Where(x => x.FID_ATTR == cabinetId).Select(x => x.DuctInsertions.Select(y => y.DuctId)).FirstAsync();
            List<ProjectAdapterBuildingModel> result = new List<ProjectAdapterBuildingModel>();

            foreach (Int32? ductID in ductIds)
            {
                if (ductID.HasValue == false) { continue; }

                TKILines line = await TKILines.FirstAsync(x => x.FID_ATTR == ductID);
                Int32 startIndex = result.Count;
                await FindBuildings(new Int32[] { line.FID }, new List<ConnectionPathElement>(), result);
                Int32 endIndex = result.Count;

                TKIDucts duct = await TKIDucts.FindAsync(ductID.Value);

                if (duct.ColorId.HasValue == true)
                {
                    for (int i = startIndex; i < endIndex; i++)
                    {
                        result[i].MicroductColor = await GetProjectColor(duct.ColorId.Value);
                    }
                }
                else
                {

                }
            }

            return result;
        }

        public async Task<IEnumerable<ProjectAdapterBranchableOverviewModel>> GetAllBranchables()
        {
            List<ProjectAdapterBranchableOverviewModel> cabinetResultr = await (from cabinet in TKIStreetCabinents
                                                                                join point in TKIPoints on cabinet.FID equals point.FID_ATTR
                                                                                orderby point.Name
                                                                                select new ProjectAdapterBranchableOverviewModel
                                                                                {
                                                                                    AdapterId = cabinet.FID.ToString(),
                                                                                    Name = point.Name,
                                                                                    Type = BranchableTypes.StreetCabinet,
                                                                                }).ToListAsync();

            List<ProjectAdapterBranchableOverviewModel> manholeResult = await (from sleeve in TKICleeves
                                                                               join point in TKIPoints on sleeve.FID equals point.FID_ATTR
                                                                               orderby point.Name
                                                                               select new ProjectAdapterBranchableOverviewModel
                                                                               {
                                                                                   AdapterId = sleeve.FID.ToString(),
                                                                                   Name = point.Name,
                                                                                   Type = BranchableTypes.Manhole,
                                                                               }).ToListAsync();

            IEnumerable<ProjectAdapterBranchableOverviewModel> combinedResult = manholeResult.Union(cabinetResultr);
            return combinedResult;
        }

        private async Task<ProjectAdapterFiberModel> GetFiberInfo(Int32 fiberID)
        {
            ProjectAdapterFiberModel result = new ProjectAdapterFiberModel { ProjectAdapterId = fiberID.ToString() };

            TKIFiber fiber = await TKIFibers.FirstAsync(x => x.FID == fiberID);
            TKILines line = await TKILines.FirstAsync(x => x.FID == fiber.LineId);
            TKICable cable = await TKICables.FirstAsync(x => x.FID == line.FID_ATTR.Value);
            TKICableModel cableModel = cable.TKICableModel;

            result.Name = line.NAME;
            result.CableProjectAdapterId = cable.FID.ToString();
            result.Color = fiber.Color;
            result.NumberinBundle = fiber.NumberInBundle.HasValue == true ? fiber.NumberInBundle.Value : -1;
            result.BundleNumber = fiber.BundleNumber.HasValue == true ? fiber.BundleNumber.Value : -1;
            if (cableModel.FiberCountPerBundle.HasValue == true)
            {
                result.FiberNumber = (cableModel.FiberCountPerBundle.Value * (result.BundleNumber-1)) + result.NumberinBundle;
            }

            if (String.IsNullOrEmpty(result.Color) == false)
            {
                Int32 indexToSplit = result.Color.IndexOf(':');
                if (indexToSplit >= 0)
                {
                    result.Color = result.Color.Substring(indexToSplit + 2);
                }

                if (cableModel.FiberCount == cableModel.FiberCountPerBundle)
                {
                    Int32 fiberIndexToSplit = result.Color.LastIndexOf('/');
                    if(fiberIndexToSplit >= 0)
                    {
                        result.Color = result.Color.Substring(0, fiberIndexToSplit);
                    }
                }
            }



            return result;
        }


        private async Task<ProjektAdapterSimpleBranchableModel> GetSimpleBranchableModel(String projectAdapterId)
        {
            Int32 branchblePoint = Convert.ToInt32(projectAdapterId);

            var preResult = await (from cabinet in TKIStreetCabinents
                                   join point in TKIPoints on cabinet.FID equals point.FID_ATTR
                                   where cabinet.FID == branchblePoint
                                   select new
                                   {
                                       AdapterId = projectAdapterId,
                                       Name = point.Name,
                                       PointId = point.FID,
                                   }).FirstAsync();

            ProjektAdapterSimpleBranchableModel result = new ProjektAdapterSimpleBranchableModel
            {
                AdapterId = preResult.AdapterId,
                Name = preResult.Name,
                Coordinate = await TKIPoint.GetCoordinate(this, preResult.PointId)
            };

            return result;
        }


        public async Task<ProjektAdapterSplicePrintResultModel> GetAllSpicesByBranchableId(String projectAdapterId)
        {
            ProjektAdapterSimpleBranchableModel branchable = await GetSimpleBranchableModel(projectAdapterId);

            Int32 pointAttributeId = Convert.ToInt32(projectAdapterId);

            Int32 branchablePointId = await TKIPoints.Where(x => x.FID_ATTR == pointAttributeId).Select(x => x.FID).FirstAsync();
            Int32 closureId = await TKIClosures.Where(x => x.StructurePointId == branchablePointId).Select(x => x.FID).FirstAsync();
            Int32 closurePointId = await TKIPoints.Where(x => x.FID_ATTR == closureId).Select(x => x.FID).FirstAsync();

            List<TKISplices> splices = await TKISplices.Where(x => x.PointId == closurePointId).ToListAsync();

            List<ProjektAdapterSplicePrintModel> result = new List<ProjektAdapterSplicePrintModel>(splices.Count);
            foreach (TKISplices splice in splices)
            {
                ProjektAdapterSplicePrintModel resultModel = new ProjektAdapterSplicePrintModel { ProjectAdapterId = splice.FID.ToString() };

                if (splice.Fiber1Id.HasValue == true && splice.Fiber2Id.HasValue == true)
                {
                    resultModel.Fiber1 = await GetFiberInfo(splice.Fiber1Id.Value);
                    resultModel.Fiber2 = await GetFiberInfo(splice.Fiber2Id.Value);

                    if (splice.FiberTrayId.HasValue == false)
                    {
                        resultModel.Type = SpliceTypes.LoopUntouched;
                    }
                    else
                    {
                        TKISpliceTray tray = await TKISpliceTrays.FirstAsync(x => x.FID == splice.FiberTrayId.Value);
                        resultModel.TrayNumber = tray.Number.HasValue == true ? tray.Number.Value : -1;

                        if (splice.NumberInTray.HasValue == false)
                        {
                            resultModel.Type = SpliceTypes.LoopTouched;
                        }
                        else
                        {
                            resultModel.Type = SpliceTypes.Normal;
                            resultModel.NumberInTray = splice.NumberInTray.Value;
                        }
                    }
                }
                else
                {
                    Int32 fiberId = splice.Fiber1Id.HasValue == true ? splice.Fiber1Id.Value : splice.Fiber2Id.Value;
                    resultModel.Fiber1 = await GetFiberInfo(fiberId);

                    if (splice.FiberTrayId.HasValue == false)
                    {
                        resultModel.Type = SpliceTypes.Unkown;
                    }
                    else
                    {
                        TKISpliceTray tray = await TKISpliceTrays.FirstAsync(x => x.FID == splice.FiberTrayId.Value);
                        resultModel.TrayNumber = tray.Number.HasValue == true ? tray.Number.Value : -1;
                        if (tray.ModelId == _blankFiberSpliceTrayModelID)
                        {
                            resultModel.Type = SpliceTypes.Buffer;
                        }

                        if (tray.Number == _depositPseudoTrayNumber)
                        {
                            resultModel.Type = SpliceTypes.Deposit;
                        }
                    }
                }

                result.Add(resultModel);
            }

            ProjektAdapterSplicePrintResultModel realResult = new ProjektAdapterSplicePrintResultModel
            {
                Branchable = branchable,
                Splices = result.OrderBy(x => x.Type).ThenBy(x => x.TrayNumber).ThenBy(x => x.NumberInTray),
            };

            return realResult;
        }

        public async Task<IEnumerable<ProjectAdapterCableTypeModel>> GetAllCableTypes()
        {
            IEnumerable<ProjectAdapterCableTypeModel> result = await TKICableModels.OrderBy(x => x.Name).Select(x => new ProjectAdapterCableTypeModel
            {
                AdapterId = x.FID.ToString(),
                FiberAmount = x.FiberCount.Value,
                FiberPerBundle = x.FiberCountPerBundle.Value,
                Name = x.Name,
            }).ToListAsync();

            return result;
        }

        private async Task<Boolean> FindPathToBranchable(Int32 currentNode, Boolean checkForLine, HashSet<TKILines> path, HashSet<TKILines> checkedPath, HashSet<TKIPoint> checkedPoints)
        {
            IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == currentNode || x.ToId == currentNode).ToListAsync();

            Boolean foundBranchable = false;
            foreach (TKIConnections item in connections)
            {
                Int32 lineId = item.FromId;
                Int32 pointId = item.ToId;
                if (item.ClassTypeFrom != ClassTypes.Line)
                {
                    lineId = item.ToId;
                    pointId = item.FromId;
                }

                TKILines line = await TKILines.FirstAsync(x => x.FID == lineId);
                TKIPoint point = await TKIPoints.FirstAsync(x => x.FID == pointId);

                if (point.ClassType == ClassTypes.DuctFinish)
                {
                    TKDuctInsertions ductInsertion = await TKDuctInsertions.FirstAsync(x => x.FID == point.FID_ATTR.Value);
                    if (ductInsertion.PointId.HasValue == true)
                    {
                        TKIPoint ductEndpoint = await TKIPoints.FirstAsync(x => x.FID == ductInsertion.PointId);
                        if (ductEndpoint.ClassType == ClassTypes.StreetCabinet)
                        {
                            return true;
                        }
                    }
                }

                if (checkForLine == true && checkedPath.Contains(line) == true) { continue; }
                if (checkForLine == false && checkedPoints.Contains(point) == true) { continue; };

                Int32 nextId = lineId;
                if (checkForLine == false)
                {
                    nextId = pointId;
                }

                checkedPath.Add(line);
                checkedPoints.Add(point);

                foundBranchable = await FindPathToBranchable(nextId, !checkForLine, path, checkedPath, checkedPoints);
                if (foundBranchable == true)
                {
                    path.Add(line);
                    return true;
                }
            }

            return foundBranchable;
        }

        private ProjectAdapterUpdateBuldingModel EmptyBuldingModel(String adapterId)
        {
            return new ProjectAdapterUpdateBuldingModel
            {
                AdapterId = adapterId,
                Length = -1,
                AdapterCableTypeId = String.Empty,
            };
        }

        public async Task<ProjectAdapterUpdateBuldingModel> GetUpdateBuildingWithCableInfo(String adapterId)
        {
            Int32 buildingId = Convert.ToInt32(adapterId);
            TKIBuilding building = await TKIBuildings.FirstAsync(x => x.FID == buildingId);

            List<TKIPoint> points = await TKIPoints.Where(x => x.FID_BUILDING.Value == building.FID && x.ClassType == ClassTypes.DuctFinish).ToListAsync();

            if (points.Count == 0)
            {
                return EmptyBuldingModel(adapterId);
            }

            Boolean found = false;
            HashSet<TKILines> pathToBranchable = new HashSet<DataAccess.TKILines>();
            foreach (TKIPoint item in points)
            {
                await FindPathToBranchable(points[0].FID, true, pathToBranchable, new HashSet<DataAccess.TKILines>(), new HashSet<TKIPoint>());
                if (pathToBranchable.Count > 0)
                {
                    break;
                }
                else
                {
                    pathToBranchable.Clear();
                }
            }

            if (pathToBranchable.Count == 0)
            {
                return EmptyBuldingModel(adapterId);
            }

            Double length = Convert.ToDouble(pathToBranchable.Sum(x => x.LENGTH.Value));

            TKILines firstLine = pathToBranchable.First();
            Int32 ductId = firstLine.FID_ATTR.Value;
            TKIDuctCableMapper mapper = await TKIDuctCableMappers.FirstAsync(x => x.DuctId.Value == ductId);
            TKILines cableLine = await TKILines.FirstAsync(x => x.FID == mapper.LineId);
            Int32 cableModelId = await TKICables.Where(x => x.FID == cableLine.FID_ATTR.Value).Select(x => x.TKICableModel.FID).FirstAsync();

            ProjectAdapterUpdateBuldingModel model = new ProjectAdapterUpdateBuldingModel
            {
                AdapterId = adapterId,
                Length = length,
                AdapterCableTypeId = cableModelId.ToString(),
            };

            return model;
        }

        public async Task<ProjectAdapterUpdateBuildingNameModel> GetUpdateBuildingName(String adaperId)
        {
            Int32 id = Convert.ToInt32(adaperId);

            TKIBuilding building = await this.TKIBuildings.FirstOrDefaultAsync(x => x.FID == id);
            if (building == null)
            {
                return new ProjectAdapterUpdateBuildingNameModel
                {
                    AdapterId = adaperId,
                    Name = String.Empty,
                };
            }

            String name = String.Empty;
            if (building.FID_STREET.HasValue == false)
            {
                name = building.NAME;
            }
            else
            {
                name = building.Street.Name + " " + building.HouseNumber;
                if (String.IsNullOrEmpty(building.SUFFIX) == false)
                {
                    name += " " + building.SUFFIX;
                }
            }

            return new ProjectAdapterUpdateBuildingNameModel
            {
                Name = name,
                AdapterId = adaperId,
            };
        }

        private async Task<String> GetCableIdByFiberId(Int32 fiberId)
        {
            TKIFiber fiber = await TKIFibers.FirstAsync(x => x.FID == fiberId);
            TKILines line = await TKILines.FirstAsync(x => x.FID == fiber.LineId);
            TKICable cable = await TKICables.FirstAsync(x => x.FID == line.FID_ATTR.Value);

            return cable.FID.ToString();
        }

        private async Task<TKICable> GetTKICableByFiberId(Int32 fiberId)
        {
            TKIFiber fiber = await TKIFibers.FirstAsync(x => x.FID == fiberId);
            TKILines line = await TKILines.FirstAsync(x => x.FID == fiber.LineId);
            TKICable cable = await TKICables.FirstAsync(x => x.FID == line.FID_ATTR.Value);

            return cable;
        }

        private async Task<ProjectAdapterCableModel> GetCableInfoByFiberId(Int32 fiberId, Int32 branchablePointId)
        {
            TKIFiber fiber = await TKIFibers.FirstAsync(x => x.FID == fiberId);
            TKILines line = await TKILines.FirstAsync(x => x.FID == fiber.LineId);
            TKICable cable = await TKICables.FirstAsync(x => x.FID == line.FID_ATTR.Value);
            TKICableModel cableModel = cable.TKICableModel;

            ProjectAdapterCableModel result = new ProjectAdapterCableModel
            {
                ProjectAdapterId = cable.FID.ToString(),
                Lenght = line.LENGTH.HasValue == true ? Convert.ToDouble(line.LENGTH.Value) : -1,
                Name = line.NAME,
                ProjectAdapterCableTypeId = cableModel.FID.ToString(),
            };

            IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.ToId == line.FID || x.FromId == line.FID).ToListAsync();

            foreach (TKIConnections connection in connections)
            {
                Int32 pointId = connection.FromId;
                if (connection.ClassTypeTo == ClassTypes.Point)
                {
                    pointId = connection.ToId;
                }

                TKIPoint point = await TKIPoints.FirstAsync(x => x.FID == pointId);
                if (point.FID_BUILDING.HasValue == true)
                {
                    result.ConnectedBuildingAdapterId = point.FID_BUILDING.Value.ToString();
                }
            }

            IEnumerable<TKIDuctCableMapper> ductMappings = await TKIDuctCableMappers.Where(x => x.LineId == line.FID && x.DuctId.HasValue == true).ToListAsync();
            List<Int32> ductsStartedAtBranchables = await TKDuctInsertions.Where(x => x.PointId == branchablePointId && x.DuctId.HasValue == true).Select(x => x.DuctId.Value).ToListAsync();

            foreach (TKIDuctCableMapper mapper in ductMappings)
            {
                if (ductsStartedAtBranchables.Contains(mapper.DuctId.Value) == true)
                {
                    TKIDucts duct = await TKIDucts.FirstAsync(x => x.FID == mapper.DuctId.Value);
                    ProjectAdapterColor color = await GetProjectColor(duct.ColorId.Value);
                    result.DuctColor = color;
                }
            }

            return result;
        }

        public async Task<IEnumerable<ProjectAdapterCableModel>> GetCableByBranchableId(String branchableProjectAdapterId)
        {
            Int32 pointAttributeId = Convert.ToInt32(branchableProjectAdapterId);

            Int32 branchablePointId = await TKIPoints.Where(x => x.FID_ATTR == pointAttributeId).Select(x => x.FID).FirstAsync();
            Int32 closureId = await TKIClosures.Where(x => x.StructurePointId == branchablePointId).Select(x => x.FID).FirstAsync();
            Int32 closurePointId = await TKIPoints.Where(x => x.FID_ATTR == closureId).Select(x => x.FID).FirstAsync();

            List<TKISplices> splices = await TKISplices.Where(x => x.PointId == closurePointId).ToListAsync();

            Dictionary<String, ProjectAdapterCableModel> cables = new Dictionary<String, ProjectAdapterCableModel>();
            foreach (TKISplices splice in splices)
            {
                ProjektAdapterSplicePrintModel resultModel = new ProjektAdapterSplicePrintModel { ProjectAdapterId = splice.FID.ToString() };

                if (splice.Fiber1Id.HasValue == true)
                {
                    ProjectAdapterCableModel cable = await GetCableInfoByFiberId(splice.Fiber1Id.Value, branchablePointId);
                    if (cables.ContainsKey(cable.ProjectAdapterId) == false)
                    {
                        cables.Add(cable.ProjectAdapterId, cable);
                    }
                }

                if (splice.Fiber2Id.HasValue == true)
                {
                    ProjectAdapterCableModel cable = await GetCableInfoByFiberId(splice.Fiber2Id.Value, branchablePointId);
                    if (cables.ContainsKey(cable.ProjectAdapterId) == false)
                    {
                        cables.Add(cable.ProjectAdapterId, cable);
                    }
                }
            }

            return cables.Values;
        }

        public async Task<IEnumerable<ProjectAdapterSpliceModel>> GetSplicesForBranchable(String projectAdapterId)
        {
            Int32 pointAttributeId = Convert.ToInt32(projectAdapterId);

            Int32 branchablePointId = await TKIPoints.Where(x => x.FID_ATTR == pointAttributeId).Select(x => x.FID).FirstAsync();
            Int32 closureId = await TKIClosures.Where(x => x.StructurePointId == branchablePointId).Select(x => x.FID).FirstAsync();
            Int32 closurePointId = await TKIPoints.Where(x => x.FID_ATTR == closureId).Select(x => x.FID).FirstAsync();

            List<TKISplices> splices = await TKISplices.Where(x => x.PointId == closurePointId).ToListAsync();

            List<ProjectAdapterSpliceModel> result = new List<ProjectAdapterSpliceModel>(splices.Count);
            foreach (TKISplices splice in splices)
            {
                ProjectAdapterSpliceModel spliceModel = new ProjectAdapterSpliceModel { ProjectAdapterId = splice.FID.ToString() };

                if (splice.Fiber1Id.HasValue == true)
                {
                    spliceModel.FirstFiber = await GetFiberInfo(splice.Fiber1Id.Value);
                }

                if (splice.Fiber2Id.HasValue == true)
                {
                    spliceModel.SecondFiber = await GetFiberInfo(splice.Fiber2Id.Value);
                }

                if (splice.Fiber1Id.HasValue == true && splice.Fiber2Id.HasValue == true)
                {
                    if (splice.FiberTrayId.HasValue == false)
                    {
                        spliceModel.Type = SpliceTypes.LoopUntouched;
                    }
                    else
                    {
                        TKISpliceTray tray = await TKISpliceTrays.FirstAsync(x => x.FID == splice.FiberTrayId.Value);
                        spliceModel.TrayNumber = tray.Number.HasValue == true ? tray.Number.Value : -1;

                        if (splice.NumberInTray.HasValue == false)
                        {
                            spliceModel.Type = SpliceTypes.LoopTouched;
                        }
                        else
                        {
                            spliceModel.Type = SpliceTypes.Normal;
                            spliceModel.NumberInTray = splice.NumberInTray.Value;
                        }
                    }
                }
                else
                {
                    Int32 fiberId = splice.Fiber1Id.HasValue == true ? splice.Fiber1Id.Value : splice.Fiber2Id.Value;

                    if (splice.FiberTrayId.HasValue == false)
                    {
                        spliceModel.Type = SpliceTypes.Unkown;
                    }
                    else
                    {
                        TKISpliceTray tray = await TKISpliceTrays.FirstAsync(x => x.FID == splice.FiberTrayId.Value);
                        spliceModel.TrayNumber = tray.Number.HasValue == true ? tray.Number.Value : -1;
                        if (tray.ModelId == _blankFiberSpliceTrayModelID)
                        {
                            spliceModel.Type = SpliceTypes.Buffer;
                        }

                        if (tray.Number == _depositPseudoTrayNumber)
                        {
                            spliceModel.Type = SpliceTypes.Deposit;
                        }
                    }
                }


                result.Add(spliceModel);
            }

            return result;
        }

        public async Task<DataTable> GetAllSpicesByBranchableIdAsDataTable(String projectAdapterId, List<string> headings)
        {
            IEnumerable<ProjektAdapterSplicePrintModel> printModels = (await GetAllSpicesByBranchableId(projectAdapterId)).Splices;

            DataTable dt = new DataTable();
            foreach (string header in headings)
            {
                dt.Columns.Add();
            }

            dt.Rows.Add(headings);

            foreach (ProjektAdapterSplicePrintModel item in printModels)
            {
                DataRow row = dt.NewRow();
                List<string> fields = new List<string>();
                fields.Add(item.Fiber1.Name);
                fields.Add(String.Format("{0}-{1}", item.Fiber1.BundleNumber, item.Fiber1.NumberinBundle));
                fields.Add(item.Fiber1.NumberinBundle.ToString());
                fields.Add(item.Fiber1.Color);

                if (item.Fiber2 != null)
                {
                    fields.Add(item.Fiber2.Name);
                    fields.Add(String.Format("{0}-{1}", item.Fiber2.BundleNumber, item.Fiber2.NumberinBundle));
                    fields.Add(item.Fiber2.NumberinBundle.ToString());
                    fields.Add(item.Fiber2.Color);
                }

                row.ItemArray = fields.ToArray();
                dt.Rows.Add(row);
            }
            return dt;
        }

        public async Task<IEnumerable<ProjectAdapterPoPModel>> GetAllPoPs()
        {
            IEnumerable<TKIStreetCabinent> pops = await TKIStreetCabinents.Where(x => x.FID_MODEL == _popStreetCabinetType).ToListAsync();

            List<ProjectAdapterPoPModel> result = new List<ProjectAdapterPoPModel>();
            foreach (TKIStreetCabinent pop in pops)
            {
                List<Int32> branchableIDs = new List<int>();

                TKIPoint popPoint = await TKIPoints.FirstAsync(x => x.FID_ATTR == pop.FID);

                IEnumerable<FiberTerminator> terminators = await FiberTerminators.Where(x => x.FID_STRUCTURE_POINT.Value == popPoint.FID && x.FID_TRUNK.HasValue == true).ToListAsync();
                foreach (FiberTerminator fiberTerminator in terminators)
                {
                    TKIPoint terminatorPoint = await TKIPoints.FirstAsync(x => x.FID_ATTR == fiberTerminator.FID);
                    String name = terminatorPoint.Name;
                    FiberCableTrunk trunk = await FiberCableTrunks.FirstAsync(x => x.FID == fiberTerminator.FID_TRUNK.Value);

                    List<TKICable> cables = await TKICables.Where(x => x.FID_TRUNK.Value == trunk.FID).ToListAsync();

                    foreach (TKICable cable in cables)
                    {
                        TKILines line = await TKILines.FirstAsync(x => x.FID_ATTR == cable.FID);
                        IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == line.FID || x.ToId == line.FID).ToListAsync();

                        foreach (TKIConnections connection in connections)
                        {
                            Int32 pointId = connection.ToId;
                            if (connection.ClassTypeFrom == ClassTypes.Point)
                            {
                                pointId = connection.FromId;
                            }

                            TKIPoint connecntionPoint = await TKIPoints.FirstAsync(x => x.FID == pointId);
                            if (connecntionPoint.ClassType == ClassTypes.Sleeve)
                            {
                                TKIClosure closure = await TKIClosures.FirstAsync(x => x.FID == connecntionPoint.FID_ATTR.Value);
                                if (closure.StructurePointId.HasValue == false) { continue; }

                                Int32 cabinetPointId = closure.StructurePointId.Value;
                                TKIPoint cabinetPoint = await TKIPoints.FirstAsync(x => x.FID == cabinetPointId);
                                if (cabinetPoint.ClassType != ClassTypes.StreetCabinet) { continue; }

                                TKIStreetCabinent cabinet = await TKIStreetCabinents.FirstAsync(x => x.FID == cabinetPoint.FID_ATTR.Value);
                                if (branchableIDs.Contains(cabinet.FID) == false)
                                {
                                    branchableIDs.Add(cabinet.FID);
                                }
                            }
                        }
                    }
                }

                ProjectAdapterPoPModel resultItem = new ProjectAdapterPoPModel
                {
                    Name = popPoint.Name,
                    ProjectAdapterId = pop.FID.ToString(),
                    Branchables = branchableIDs.Select(x => x.ToString())
                };

                resultItem.Location = await TKIPoint.GetCoordinate(this, popPoint.FID);
                result.Add(resultItem);
            }

            return result;
        }

        public async Task<IEnumerable<String>> GetMainCableStartingFromPop(String popProjectAdapterId)
        {
            Int32 popFid = Convert.ToInt32(popProjectAdapterId);
            TKIStreetCabinent pop = await TKIStreetCabinents.FirstAsync(x => x.FID == popFid);

            TKIPoint popPoint = await TKIPoints.FirstAsync(x => x.FID_ATTR == pop.FID);

            HashSet<Int32> startingCables = new HashSet<int>();

            IEnumerable<FiberTerminator> terminators = await FiberTerminators.Where(x => x.FID_STRUCTURE_POINT.Value == popPoint.FID && x.FID_TRUNK.HasValue == true).ToListAsync();
            foreach (FiberTerminator fiberTerminator in terminators)
            {
                TKIPoint terminatorPoint = await TKIPoints.FirstAsync(x => x.FID_ATTR == fiberTerminator.FID);
                IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == terminatorPoint.FID || x.ToId == terminatorPoint.FID).ToArrayAsync();

                foreach (TKIConnections connecton in connections)
                {
                    Int32 lineId = connecton.FromId;
                    if (connecton.ClassTypeTo == ClassTypes.Line)
                    {
                        lineId = connecton.ToId;
                    }

                    TKILines line = await TKILines.FirstAsync(x => x.FID == lineId);
                    if (line.ClassType != ClassTypes.Cable) { continue; }

                    Int32 cableId = line.FID_ATTR.Value;
                    TKICable cable = await TKICables.FirstAsync(x => x.FID == cableId);

                    startingCables.Add(cable.FID);
                }
            }

            return startingCables.Select(x => x.ToString());
        }

        private async Task<String> FiberHelper(HashSet<Int32> checkedLines, Int32 lineOrPointId, Boolean isPoint)
        {
            IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == lineOrPointId || x.ToId == lineOrPointId).ToListAsync();

            foreach (TKIConnections connection in connections)
            {
                if (checkedLines.Contains(connection.FID) == true) { continue; }

                checkedLines.Add(connection.FID);

                Int32 elementId = connection.ToId;
                if (isPoint == false)
                {
                    if (connection.ClassTypeFrom == ClassTypes.Point)
                    {
                        elementId = connection.FromId;
                    }
                }
                else
                {
                    if (connection.ClassTypeFrom == ClassTypes.Line)
                    {
                        elementId = connection.FromId;
                    }
                }

                if (isPoint == false)
                {
                    TKIPoint point = await TKIPoints.FirstAsync(x => x.FID == elementId);
                    if (point.ClassType == ClassTypes.Splice)
                    {
                        String result = await FiberHelper(checkedLines, point.FID, true);
                        if (String.IsNullOrEmpty(result) == false) { return result; }
                    }
                    else if (point.ClassType == ClassTypes.FiberFinish && point.FID_BUILDING.HasValue == false)
                    {
                        FiberConnector connector = await FiberConnectors.FirstOrDefaultAsync(x => x.FID == point.FID_ATTR.Value);
                        if (connector == null) { continue; }

                        String name = connector.NAME;
                        return name;

                        //TKIPoint terminationPoint = await TKIPoints.FirstAsync(x => x.FID == connector.FID_POINT.Value);
                        //FiberTerminator terminator = await FiberTerminators.FirstAsync(x => x.FID == terminationPoint.FID_ATTR.Value);

                        //if(terminator.FID_STRUCTURE_POINT.HasValue == true)
                        //{
                        //    TKIPoint popPoint = await TKIPoints.FirstAsync(x => x.FID == terminator.FID_STRUCTURE_POINT.Value);
                        //    TKIStreetCabinent cabinet = await TKIStreetCabinents.FirstAsync(x => x.FID == popPoint.FID_ATTR.Value);
                        //}
                    }
                }
                else
                {
                    TKILines line = await TKILines.FirstAsync(x => x.FID == elementId);
                    if (line.ClassType == ClassTypes.Fiber)
                    {
                        String result = await FiberHelper(checkedLines, line.FID, false);
                        if (String.IsNullOrEmpty(result) == false) { return result; }
                    }
                }
            }

            return String.Empty;
        }

        public async Task<IEnumerable<ProjectAdapterPatchModel>> GetConnectionByCableId(String projectAdatperCableID)
        {
            Int32 startCableId = Convert.ToInt32(projectAdatperCableID);
            TKICable startCable = await TKICables.FirstAsync(x => x.FID == startCableId);
            TKILines startCableLine = await TKILines.FirstAsync(x => x.FID_ATTR.Value == startCableId);

            IEnumerable<TKIFiber> fibers = await TKIFibers.Where(x => x.LineId.Value == startCableLine.FID).ToListAsync();

            List<ProjectAdapterPatchModel> result = new List<ProjectAdapterPatchModel>();
            foreach (TKIFiber fiber in fibers)
            {
                TKILines fiberLine = await TKILines.FirstAsync(x => x.FID_ATTR.Value == fiber.FID);
                IEnumerable<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == fiberLine.FID || x.ToId == fiberLine.FID).ToListAsync();

                HashSet<Int32> checkedLines = new HashSet<int>();
                String connenctioName = await FiberHelper(checkedLines, fiberLine.FID, false);
                if (String.IsNullOrEmpty(connenctioName) == false)
                {
                    ProjectAdapterPatchModel resultItem = new ProjectAdapterPatchModel();
                    resultItem.Caption = connenctioName;

                    // NA_K_02_03_04
                    String[] parts = connenctioName.Split('_');

                    Int32 columnIndex = parts.Length - 1;
                    Int32 rowIndex = parts.Length - 2;
                    Int32 moduleIndex = parts.Length - 3;

                    if(columnIndex >= 0)
                    {
                        resultItem.ColumnNumber = parts[columnIndex];
                    }
                    if(rowIndex >= 0)
                    {
                        resultItem.RowNumber = parts[rowIndex];
                    }
                    if (moduleIndex >= 0)
                    {
                        resultItem.ModuleName = parts[moduleIndex];
                    }

                    result.Add(resultItem);
                }
            }

            return result;
        }

        private async Task GetCalbePath(Int32 pointOrLineID, Boolean isPoint, ICollection<Int32> usedPath, ICollection<TKICable> realPath, Int32 trunkID)
        {
            List<TKIConnections> connections = await TKIConnections.Where(x => x.ToId == pointOrLineID || x.FromId == pointOrLineID).ToListAsync();

            foreach (TKIConnections connection in connections)
            {
                if (usedPath.Contains(connection.FID) == true) { continue; }
                usedPath.Add(connection.FID);


                Int32 nextLineOrPointId = connection.FromId;
                Int32 currentLineId = connection.ToId;
                if (isPoint == true)
                {
                    if (connection.ClassTypeTo == ClassTypes.Line)
                    {
                        nextLineOrPointId = connection.ToId;
                    }
                }
                else if (isPoint == false)
                {
                    if (connection.ClassTypeTo == ClassTypes.Point)
                    {
                        nextLineOrPointId = connection.ToId;
                    }
                }

                if (connection.ClassTypeFrom == ClassTypes.Line)
                {
                    currentLineId = connection.FromId;
                }

                TKILines line = await TKILines.FirstAsync(x => x.FID == currentLineId);

                if (line.ClassType != ClassTypes.Cable)
                {
                    continue;
                }

                TKICable cable = await TKICables.FirstAsync(x => x.FID == line.FID_ATTR.Value);
                if (cable.FID_TRUNK.HasValue == false || cable.FID_TRUNK.Value != trunkID) { continue; }

                realPath.Add(cable);

                await GetCalbePath(nextLineOrPointId, !isPoint, usedPath, realPath, trunkID);
            }
        }

        public async Task<ProjectAdapterMainCableForBranchableResult> GetMainCableForBranchable(String branchableProjectAdapterId)
        {
            String mainCableId = String.Empty;

            Int32 pointAttributeId = Convert.ToInt32(branchableProjectAdapterId);

            TKIPoint branchablePoint = await TKIPoints.Where(x => x.FID_ATTR == pointAttributeId).FirstAsync();
            Int32 closureId = await TKIClosures.Where(x => x.StructurePointId == branchablePoint.FID).Select(x => x.FID).FirstAsync();
            Int32 closurePointId = await TKIPoints.Where(x => x.FID_ATTR == closureId).Select(x => x.FID).FirstAsync();

            List<TKISplices> splices = await TKISplices.Where(x => x.PointId == closurePointId).ToListAsync();

            List<TKICable> calbes = new List<TKICable>();
            foreach (TKISplices splice in splices)
            {
                ProjektAdapterSplicePrintModel resultModel = new ProjektAdapterSplicePrintModel { ProjectAdapterId = splice.FID.ToString() };

                if (splice.Fiber1Id.HasValue == true)
                {
                    TKICable cable = await GetTKICableByFiberId(splice.Fiber1Id.Value);
                    if (cable.FID_TRUNK.HasValue == true)
                    {
                        calbes.Add(cable);
                    }

                }

                if (splice.Fiber2Id.HasValue == true)
                {
                    TKICable cable = await GetTKICableByFiberId(splice.Fiber2Id.Value);
                    if (cable.FID_TRUNK.HasValue == true)
                    {
                        calbes.Add(cable);
                    }
                }
            }

            HashSet<Int32> trunkIds = new HashSet<int>(calbes.Where(x => x.FID_TRUNK.HasValue == true).Select(x => x.FID_TRUNK.Value));

            foreach (Int32 item in trunkIds)
            {
                FiberTerminator terminator = await FiberTerminators.FirstOrDefaultAsync(x => x.FID_TRUNK.Value == item);
                if (terminator == null) { continue; }

                TKIPoint point = await TKIPoints.FirstAsync(x => x.FID_ATTR == terminator.FID);

                List<Int32> usedConnections = new List<Int32>();
                HashSet<TKICable> cablePath = new HashSet<TKICable>();

                await GetCalbePath(point.FID, true, usedConnections, cablePath, item);

                foreach (TKICable cable in cablePath)
                {
                    if (calbes.Contains(cable) == true)
                    {
                        mainCableId = cable.FID.ToString();
                        break;
                    }
                }
            }

            String secondBranchableId = String.Empty;

            if (String.IsNullOrEmpty(mainCableId) == false)
            {
                Int32 mianCableIdAsInt = Convert.ToInt32(mainCableId);
                TKICable mainCable = await TKICables.FirstAsync(x => x.FID == mianCableIdAsInt);

                TKILines mainCableLine = await TKILines.FirstAsync(X => X.FID_ATTR == mainCable.FID);

                List<TKIConnections> connections = await TKIConnections.Where(x => x.FromId == mainCableLine.FID || x.ToId == mainCableLine.FID).ToListAsync();
                foreach (TKIConnections item in connections)
                {
                    if (item.ToId != closurePointId && item.FromId != closurePointId)
                    {
                        Int32 pointId = item.ToId;
                        if (item.ClassTypeTo != ClassTypes.Point)
                        {
                            pointId = item.FromId;
                        }

                        TKIPoint secondClosurePoint = await TKIPoints.FirstAsync(X => X.FID == pointId);
                        if (secondClosurePoint.ClassType == ClassTypes.Sleeve)
                        {
                            TKIClosure secondClousure = await TKIClosures.FirstAsync(x => x.FID == secondClosurePoint.FID_ATTR);

                            if (secondClousure.StructurePointId.HasValue == true)
                            {
                                TKIPoint secondBranchablePoint = await TKIPoints.FirstAsync(x => x.FID == secondClousure.StructurePointId.Value);

                                if (secondBranchablePoint.FID_ATTR.HasValue == true)
                                {
                                    secondBranchableId = secondBranchablePoint.FID_ATTR.Value.ToString();
                                }
                            }
                        }
                    }
                }
            }

            return new ProjectAdapterMainCableForBranchableResult
            {
                ProjectAdapterCableId = mainCableId,
                ProjectAdapterBranchableId = branchableProjectAdapterId,
                BranchableEndCableProejctAdapterId = secondBranchableId
            };
        }

        public class SimpleCableModel
        {
            public String Name { get; set; }
            public Int32 FiberAmount { get; set; }
        }

        public async Task<Dictionary<String, List<SimpleCableModel>>> GetCableOverview()
        {
            var popResult = await GetAllPoPs();

            Dictionary<String, List<SimpleCableModel>> result = new Dictionary<string, List<SimpleCableModel>>();

            foreach (var pop in popResult)
            {
                List<SimpleCableModel> cables = new List<SimpleCableModel>();
                result.Add(pop.Name, cables);

                var cableResult = await GetMainCableStartingFromPop(pop.ProjectAdapterId);

                foreach (String cableId in cableResult)
                {
                    Int32 realId = Convert.ToInt32(cableId);
                    TKICable cable = await TKICables.FirstAsync(x => x.FID == realId);
                    TKILines line = await TKILines.FirstAsync(x => x.FID_ATTR == realId);

                    Int32 fiberAmount = cable.TKICableModel.FiberCount.Value;

                    SimpleCableModel model = new SimpleCableModel
                    {
                        Name = line.NAME,
                        FiberAmount = fiberAmount,
                    };

                    cables.Add(model);
                }
            }

            return result;
        }
    }
}
