﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterSpliceModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public ProjectAdapterFiberModel FirstFiber { get; set; }
        public ProjectAdapterFiberModel SecondFiber { get; set; }
        public Int32 TrayNumber { get; set; }
        public Int32 NumberInTray { get; set; }
        public SpliceTypes Type { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterSpliceModel()
        {

        }

        #endregion
    }
}
