﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterUpdateBuldingModel
    {
        #region Properties

        public String AdapterId { get; set; }
        public Double Length { get; set; }
        public String AdapterCableTypeId { get; set; }

        #endregion
    }
}
