﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterMainCableForBranchableResult
    {
        #region Properties

        public String ProjectAdapterBranchableId { get; set; }
        public String ProjectAdapterCableId { get; set; }
        public String BranchableEndCableProejctAdapterId;

        #endregion

        #region Constructor

        public ProjectAdapterMainCableForBranchableResult()
        {

        }

        #endregion
    }
}
