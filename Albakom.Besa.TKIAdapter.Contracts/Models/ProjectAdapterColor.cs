﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterColor
    {
        #region Properties

        public String Name { get; set; }
        public String RGBHexValue { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterColor()
        {

        }

        #endregion

        #region Custom 

        public override string ToString()
        {
            return String.Format("{0} - {1}", Name, RGBHexValue);
        }

        #endregion
    }
}
