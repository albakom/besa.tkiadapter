﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class GPSCoordinate
    {
        #region Properties

        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        #endregion

        #region Constructor

        public GPSCoordinate()
        {

        }

        #endregion

        #region Custom

        public override string ToString()
        {
            return String.Format("{0} ; {1} ", Latitude, Longitude);
        }

        #endregion
    }
}
