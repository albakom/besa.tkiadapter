﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterCableTypeModel
    {
        #region Properties

        public String AdapterId { get; set; }
        public String Name { get; set; }
        public Int32 FiberAmount { get; set; }
        public Int32 FiberPerBundle { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterCableTypeModel()
        {

        }

        #endregion
    }
}
