﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterPatchModel
    {
        #region Properties

        public String ModuleName { get; set; }
        public String RowNumber { get; set; }
        public String ColumnNumber { get; set; }

        public String Caption { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterPatchModel()
        {

        }

        #endregion
    }
}
