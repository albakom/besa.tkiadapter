﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjektAdapterSplicePrintModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public SpliceTypes Type { get; set; }
        public ProjectAdapterFiberModel Fiber1 { get; set; }
        public ProjectAdapterFiberModel Fiber2 { get; set; }
        public Int32 TrayNumber { get; set; }
        public Int32 NumberInTray { get; set; }

        #endregion

        #region Constructor

        public ProjektAdapterSplicePrintModel()
        {

        }

        #endregion

    }
}
