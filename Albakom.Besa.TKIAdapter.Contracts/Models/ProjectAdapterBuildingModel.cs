﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterBuildingModel
    {
        #region Properties

        public String AdapterId { get; set; }
        public String Street { get; set; }
        public Int32 HouseholdUnits { get; set; }
        public Int32 CommercialUnits { get; set; }
        public GPSCoordinate Coordinate { get; set; }

        public ProjectAdapterColor MicroductColor { get; set; }
        public ProjectAdapterColor HouseConnectionColor { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterBuildingModel()
        {

        }

        #endregion

        #region Override

        public override string ToString()
        {
            return String.Format("{0} - {1}", AdapterId, Street);
        }

        #endregion
    }
}
