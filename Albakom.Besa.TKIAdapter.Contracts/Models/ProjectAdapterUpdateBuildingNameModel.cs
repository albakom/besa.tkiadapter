﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterUpdateBuildingNameModel
    {
        #region Properties

        public String AdapterId { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterUpdateBuildingNameModel()
        {

        }

        #endregion
    }
}
