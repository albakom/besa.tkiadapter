﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterPoPModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Location { get; set; }

        public IEnumerable<String> Branchables { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterPoPModel()
        {

        }

        #endregion
    }
}
