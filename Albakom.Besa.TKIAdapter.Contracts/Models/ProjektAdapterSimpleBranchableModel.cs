﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjektAdapterSimpleBranchableModel
    {
        #region Properites

        public String AdapterId { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Coordinate { get; set; }

        #endregion

        #region Constructor

        public ProjektAdapterSimpleBranchableModel()
        {

        }

        #endregion
    }
}
