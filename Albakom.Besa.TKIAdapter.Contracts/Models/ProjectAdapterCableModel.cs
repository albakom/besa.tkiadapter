﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterCableModel
    {
        #region Properties

        public String ProjectAdapterId { get; set; }
        public String Name { get; set; }
        public String ProjectAdapterCableTypeId { get; set; }
        public ProjectAdapterColor DuctColor { get; set; }
        public Double Lenght { get; set; }

        public String ConnectedBuildingAdapterId { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterCableModel()
        {

        }

        #endregion
    }
}
