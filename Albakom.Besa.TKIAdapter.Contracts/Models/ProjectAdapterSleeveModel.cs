﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjectAdapterSleeveModel
    {
        #region Properties

        public String AdapterId { get; set; }
        public String Name { get; set; }
        public GPSCoordinate Coordinate { get; set; }
        public IEnumerable<ProjectAdapterBuildingModel> Buildings { get; set; }

        #endregion

        #region Constructor

        public ProjectAdapterSleeveModel()
        {
            Buildings = new List<ProjectAdapterBuildingModel>();
        }

        #endregion
    }
}
