﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.TKIAdapter.Contracts.Models
{
    public class ProjektAdapterSplicePrintResultModel
    {
        #region Properties

        public ProjektAdapterSimpleBranchableModel Branchable { get; set; }
        public IEnumerable<ProjektAdapterSplicePrintModel> Splices { get; set; }

        #endregion

        #region Constructor

        public ProjektAdapterSplicePrintResultModel()
        {

        }

        #endregion
    }
}
