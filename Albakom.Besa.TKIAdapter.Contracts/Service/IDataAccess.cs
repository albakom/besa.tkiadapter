﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Albakom.Besa.TKIAdapter.Contracts.Models;

namespace Albakom.Besa.TKIAdapter.Contracts.Service
{
    public interface IDataAccess
    {
        Task<IEnumerable<ProjectAdapterSleeveModel>> GetAllSleeves(Int32? start, Int32? amount);
        Task<IEnumerable<ProjectAdapterCabinetModel>> GetAllCabinets(Int32? start, Int32? amount);
        Task<IEnumerable<ProjectAdapterBuildingModel>> GetAllBuildings();
        Task<IEnumerable<ProjectAdapterBranchableOverviewModel>> GetAllBranchables();
        Task<IEnumerable<ProjectAdapterCableTypeModel>> GetAllCableTypes();
        Task<ProjectAdapterUpdateBuldingModel> GetUpdateBuildingWithCableInfo(String adapterId);
        Task<ProjectAdapterUpdateBuildingNameModel> GetUpdateBuildingName(String adapterId);

        Task<ProjektAdapterSplicePrintResultModel> GetAllSpicesByBranchableId(String projectAdapterId);

        Task<IEnumerable<ProjectAdapterCableModel>> GetCableByBranchableId(String branchableProjectAdapterId);
        Task<IEnumerable<ProjectAdapterSpliceModel>> GetSplicesForBranchable(String projectAdapterId);
        Task<IEnumerable<ProjectAdapterPoPModel>> GetAllPoPs();
        Task<IEnumerable<String>> GetMainCableStartingFromPop(String popProjectAdapterId);
        Task<IEnumerable<ProjectAdapterPatchModel>> GetConnectionByCableId(String projectAdatperCableID);
        Task<ProjectAdapterMainCableForBranchableResult> GetMainCableForBranchable(String branchableProjectAdapterId);

        Task<System.Data.DataTable> GetAllSpicesByBranchableIdAsDataTable(String projectAdapterId, List<string> headings);
    }
}
