﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Albakom.Besa.TKIAdapter.Contracts.Service
{
    public interface IProjectDataAdapter
    {
        Task<IEnumerable<ProjectAdapterBuildingModel>> GetAllBuildings();
        Task<IEnumerable<ProjectAdapterSleeveModel>> GetAllSleeves(Int32? start, Int32? amount);
        Task<IEnumerable<ProjectAdapterCabinetModel>> GetAllCabinets(Int32? start, Int32? amount);
        Task<IEnumerable<ProjectAdapterBranchableOverviewModel>> GetAllBranchables();
        Task<ProjektAdapterSplicePrintResultModel> GetAllSpicesByBranchableId(String projectAdapterId);
        Task<Stream> ExpportSpliceOverviewAsCSV(String projectAdapterId);
        Task<Stream> ExpportSpliceOverviewAsXlsx(String projectAdapterId);
        Task<IEnumerable<ProjectAdapterCableTypeModel>> GetAllCableTypes();
        Task<ProjectAdapterUpdateBuldingModel> GetUpdateBuildingWithCableInfo(String adapterId);
        Task<ProjectAdapterUpdateBuildingNameModel> GetUpdateBuildingName(String adapterId);

        Task<IEnumerable<ProjectAdapterCableModel>> GetCableByBranchableId(String branchableProjectAdapterId);
        Task<IEnumerable<ProjectAdapterSpliceModel>> GetSplicesForBranchable(String projectAdapterId);
        Task<IEnumerable<ProjectAdapterPoPModel>> GetAllPoPs();
        Task<IEnumerable<String>> GetMainCableStartingFromPop(String popProjectAdapterId);
        Task<IEnumerable<ProjectAdapterPatchModel>> GetConnectionByCableId(String projectAdatperCableID);
        Task<ProjectAdapterMainCableForBranchableResult> GetMainCableForBranchable(String branchableProjectAdapterId);
    }
}
