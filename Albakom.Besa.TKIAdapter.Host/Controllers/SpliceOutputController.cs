﻿using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    public class SpliceOutputController : Controller
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public SpliceOutputController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        // GET: SpliceOutput
        public async Task<ViewResult> Index()
        {
            IEnumerable<ProjectAdapterBranchableOverviewModel> branchables = await _adapter.GetAllBranchables();
            return View(branchables);
        }

        [HttpGet]
        public async Task<FileResult> GenerateFile(String id)
        {
            IEnumerable<ProjectAdapterBranchableOverviewModel> branchables = await _adapter.GetAllBranchables();
            ProjectAdapterBranchableOverviewModel corresponding = branchables.FirstOrDefault(x => x.AdapterId == id);

            String name = corresponding.Name;

            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Stream result = await _adapter.ExpportSpliceOverviewAsXlsx(id);
            return base.File(result, contentType, String.Format("{0}.xlsx", name));

            //var contentType_old = "text/csv";
            //Stream result = await _adapter.ExpportSpliceOverviewAsCSV(id);
            //return base.File(result, contentType_old, String.Format("{0}.csv", name));
        }
    }
}