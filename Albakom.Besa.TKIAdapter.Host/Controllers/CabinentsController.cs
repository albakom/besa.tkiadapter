﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    [Authorize]
    public class CabinentsController : ApiController
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public CabinentsController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterCabinetModel>> All([FromUri] Int32? start, [FromUri]Int32? amount)
        {
            IEnumerable<ProjectAdapterCabinetModel> result = await _adapter.GetAllCabinets(start, amount);
            return result;
        }
    }
}
