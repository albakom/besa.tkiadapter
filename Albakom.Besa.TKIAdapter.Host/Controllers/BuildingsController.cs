﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    [Authorize]
    public class BuildingsController : ApiController
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public BuildingsController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterBuildingModel> >All()
        {
            IEnumerable<ProjectAdapterBuildingModel> result = await _adapter.GetAllBuildings();
            return result;
        }


        [HttpGet]
        public async Task<ProjectAdapterUpdateBuldingModel> UpdateWithCable(String id)
        {
            ProjectAdapterUpdateBuldingModel result = await _adapter.GetUpdateBuildingWithCableInfo(id);
            return result;
        }

        [HttpGet]
        public async Task<ProjectAdapterUpdateBuildingNameModel> UpdateName(String id)
        {
            ProjectAdapterUpdateBuildingNameModel result = await _adapter.GetUpdateBuildingName(id);
            return result;
        }
        



    }
}
