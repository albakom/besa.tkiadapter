﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    [Authorize]
    public class CablesController : ApiController
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public CablesController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterCableTypeModel>> AllTypes()
        {
            IEnumerable<ProjectAdapterCableTypeModel> result = await _adapter.GetAllCableTypes();
            return result;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterCableModel>> CableByBranchableId(String id)
        {
            IEnumerable<ProjectAdapterCableModel> result = await _adapter.GetCableByBranchableId(id);
            return result;
        }

        [HttpGet]
        public async Task<IEnumerable<String>> MainCableStartingFromPop(String id)
        {
            IEnumerable<String> result = await _adapter.GetMainCableStartingFromPop(id);
            return result;
        }

        [HttpGet]
        public async Task<ProjectAdapterMainCableForBranchableResult> MainCableForBranchable(String id)
        {
            ProjectAdapterMainCableForBranchableResult result = await _adapter.GetMainCableForBranchable(id);
            return result;
        }
    }
}
