﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    [Authorize]
    public class SplicesController : ApiController
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public SplicesController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterSpliceModel>> ByBranchableId(String id)
        {
            IEnumerable<ProjectAdapterSpliceModel> result = await _adapter.GetSplicesForBranchable(id);
            return result;
        }

    }
}