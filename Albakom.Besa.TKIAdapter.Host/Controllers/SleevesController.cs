﻿using Albakom.Besa.TKIAdapter.Contracts.Models;
using Albakom.Besa.TKIAdapter.Contracts.Service;
using Albakom.Besa.TKIAdapter.DataAccess;
using Albakom.Besa.TKIAdapter.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Albakom.Besa.TKIAdapter.Host.Controllers
{
    [Authorize]
    public class SleevesController : ApiController
    {
        #region Fields

        private IProjectDataAdapter _adapter;

        #endregion

        #region Constructor

        public SleevesController()
        {
            IDataAccess access = new TKIEntities();
            _adapter = new ProjectDataAdapter(access);
        }

        #endregion

        [HttpGet]
        public async Task<IEnumerable<ProjectAdapterSleeveModel>> All([FromUri]Int32? start,[FromUri]Int32? amount)
        {
            IEnumerable<ProjectAdapterSleeveModel> result = await _adapter.GetAllSleeves(start,amount);
            return result;
        }
    }
}
