﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using IdentityServer3.AccessTokenValidation;

[assembly: OwinStartup(typeof(Albakom.Besa.TKIAdapter.Host.Startup))]

namespace Albakom.Besa.TKIAdapter.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);

            var blub = new IdentityServerBearerTokenAuthenticationOptions
            {
#if DEBUG
                Authority = "http://localhost:5000",
#else
                Authority = "https://besa-auth.albakom.de",
#endif
                RequiredScopes = new[] { "tki-adapter-test", "tki-adapter" }
            };

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
#if DEBUG
                Authority = "http://localhost:5000",
#else
                Authority = "http://besa-auth.albakom.de",
#endif
                RequiredScopes = new[] { "tki-adapter-test", "tki-adapter" }
            });
        }
    }
}
